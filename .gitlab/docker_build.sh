#!/usr/bin/env bash

CTX="$1"
IMG="$2"

shift 2

DEPENDS=()
CACHE=()
ARG_MODE=

for arg in "$@"
do
  if [[ "$arg" == "--depends-on" ]]; then
    ARG_MODE="deps"
  elif [[ "$arg" == "--cache-from" ]]; then
    ARG_MODE="cache"
  else
    if [[ "$ARG_MODE" == "deps" ]]; then
      DEPENDS+=("$arg")
    elif [[ "$ARG_MODE" == "cache" ]]; then
      CACHE+=("$arg")
    fi
  fi
done

set -e
set -x

if git diff-tree --quiet HEAD^ HEAD .gitlab-ci.yml .gitlab "$CTX" "${DEPENDS[@]}"; then
  echo "Skipping $IMG as there were no changes in CI/$CTX"
else

  cd "$CTX"

  docker pull "$IMG":latest || true

  for d in "${CACHE[@]}"; do
    docker pull "$IMG":"$d" || true
  done

  for d in "${CACHE[@]}"; do
    docker build ${CACHE[@]/#/--cache-from="$IMG":} --tag "$IMG":"$d" --target "$d" .
  done

  docker build ${CACHE[@]/#/--cache-from="$IMG":} --cache-from "$IMG":latest --tag "$IMG":latest .

  docker push "$IMG":latest

  for d in "${CACHE[@]}"; do
    docker push "$IMG":"$d"
  done

fi
