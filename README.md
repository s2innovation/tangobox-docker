# tangobox-docker
Dockerfiles for https://github.com/tango-controls/tangobox/

## `tango-source-distribution` container

This container provides binary packages for
[TangoSourceDistribution](https://github.com/tango-controls/TangoSourceDistribution)
and
[PyTango](https://github.com/tango-controls/pytango)
compatible with `ubuntu:bionic`.

Tango is configured with prefix `/usr/local`.

Database schema creation scripts are located in `/usr/local/share/tango/db`.

The image is available in a public registry:
```
registry.gitlab.com/s2innovation/tangobox-docker/tango-source-distribution
```

Packages can be extracted from the image as follows:
```bash
ID=$(docker create registry.gitlab.com/s2innovation/tangobox-docker/tango-source-distribution:latest)
docker cp $ID:/tango-source-distribution_9.3.3-SNAPSHOT_amd64.deb .
docker cp $ID:/pytango-9.3.0-cp27-cp27mu-linux_x86_64.whl .
docker rm $ID
```

Use `dpkg` and `pip` to install the packages:
```bash
sudo dpkg -i ./tango-source-distribution_9.3.3-SNAPSHOT_amd64.deb
sudo pip install ./pytango-9.3.0-cp27-cp27mu-linux_x86_64.whl
```
